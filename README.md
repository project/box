Module description
------------------
Box is a content entity, very similar to the custom block entity provided by Drupal core.

Install notes
-------------
The module requires a Drupal core patch for the machine name widget.
https://www.drupal.org/project/drupal/issues/2685749

Additional features (compared to custom blocks)
-------------------------------------------------
- Box uses machine name as id
- Box bundles have separate set of permissions

Additional modules (included)
-----------------------------
- box_clone: Duplicate a box entity with all its fields.
- insert_box: Text filter to display a box by machine name (or box id).
