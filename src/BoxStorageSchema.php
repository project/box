<?php

namespace Drupal\box;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the box schema handler.
 */
class BoxStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE): array {
    $schema = parent::getEntitySchema($entity_type, $reset);

    // @todo
    /*if ($data_table = $this->storage->getDataTable()) {
      $schema[$data_table]['indexes'] += [
        'box__label_type' => ['label', ['type', 4]],
      ];
    }*/

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping): array {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

    if (in_array($table_name, ['box_revision', 'biox_revision'])) {
      switch ($field_name) {
        case 'langcode':
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;

        case 'revision_uid':
          $this->addSharedTableFieldForeignKey($storage_definition, $schema, 'users', 'uid');
          break;
      }
    }

    if (in_array($table_name, ['box_field_data', 'biox_field_data'])) {
      switch ($field_name) {
        // @todo
        /*case 'status':
        case 'label':
          // Improves the performance of the indexes defined in getEntitySchema().
          $schema['fields'][$field_name]['not null'] = TRUE;
          break;*/

        case 'changed':
        case 'created':
          // @todo Revisit index definitions:
          //   https://www.drupal.org/node/2015277.
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;
      }
    }

    return $schema;
  }

}
