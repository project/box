<?php

namespace Drupal\box\Controller;

use Drupal\box\BoxStorageInterface;
use Drupal\box\Entity\BoxInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BoxController.
 *
 *  Returns responses for Box routes.
 *
 * @package Drupal\box\Controller
 */
class BoxController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * Constructs a BoxController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer, EntityRepositoryInterface $entity_repository) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BoxController {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer'),
      $container->get('entity.repository')
    );
  }

  /**
   * Displays a Box  revision.
   *
   * @param int $box_revision
   *   The Box revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow(int $box_revision): array {
    $box = $this->entityTypeManager()
      ->getStorage('box')
      ->loadRevision($box_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('box');

    return $view_builder->view($box);
  }

  /**
   * Page title callback for a Box  revision.
   *
   * @param int $box_revision
   *   The Box  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle(int $box_revision): string {
    /** @var \Drupal\box\Entity\BoxInterface $box */
    $box = $this->entityTypeManager()
      ->getStorage('box')
      ->loadRevision($box_revision);
    return $this->t('Revision of %box_label from %date', [
      '%box_label' => $box->label(),
      '%date' => $this->dateFormatter->format($box->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Box .
   *
   * @param \Drupal\box\Entity\BoxInterface $box
   *   A Box  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(BoxInterface $box): array {
    $account = $this->currentUser();
    $langcode = $box->language()->getId();
    $langname = $box->language()->getName();
    $languages = $box->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    /** @var \Drupal\box\BioxStorage $box_storage */
    $box_storage = $this->entityTypeManager()->getStorage('box');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $box->label(),
    ]) : $this->t('Revisions for %title', ['%title' => $box->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all box revisions") || $account->hasPermission('administer box entities')));
    $delete_permission = (($account->hasPermission("delete all box revisions") || $account->hasPermission('administer box entities')));

    $rows = [];
    $default_revision = $box->getRevisionId();
    $current_revision_displayed = FALSE;

    foreach ($this->getRevisionIds($box, $box_storage) as $vid) {
      /** @var \Drupal\box\Entity\BoxInterface $revision */
      $revision = $box_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)
          ->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');

        // We treat also the latest translation-affecting revision as current
        // revision, if it was the default revision, as its values for the
        // current language will be the same of the current default revision in
        // this case.
        $is_current_revision = $vid == $default_revision || (!$current_revision_displayed && $revision->wasDefaultRevision());
        if (!$is_current_revision) {
          $link = Link::fromTextAndUrl($date, new Url('entity.box.revision', [
            'box' => $box->id(),
            'box_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $box->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($is_current_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
                Url::fromRoute('entity.box.translation_revert', [
                  'box' => $box->id(),
                  'box_revision' => $vid,
                  'langcode' => $langcode,
                ]) :
                Url::fromRoute('entity.box.revision_revert', [
                  'box' => $box->id(),
                  'box_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.box.revision_delete', [
                'box' => $box->id(),
                'box_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['box_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * Gets a list of box revision IDs for a specific box.
   *
   * @param \Drupal\box\Entity\BoxInterface $box
   *   The box entity.
   * @param \Drupal\box\BoxStorageInterface $box_storage
   *   The box storage handler.
   *
   * @return int[]
   *   Box revision IDs (in descending order).
   */
  protected function getRevisionIds(BoxInterface $box, BoxStorageInterface $box_storage): array {
    $result = $box_storage->getQuery()
      ->accessCheck(TRUE)
      ->allRevisions()
      ->condition($box->getEntityType()->getKey('id'), $box->id())
      ->sort($box->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();
    return array_keys($result);
  }

}
