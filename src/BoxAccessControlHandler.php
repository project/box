<?php

namespace Drupal\box;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Box entity.
 *
 * @see \Drupal\box\Entity\Box.
 */
class BoxAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * Map of revision operations.
   *
   * Keys contain revision operations, where values are an array containing the
   * permission operation and entity operation.
   *
   * Permission operation is used to build the required permission, e.g.
   * 'permissionOperation all revisions', 'permissionOperation type revisions'.
   *
   * Entity operation is used to determine access, e.g for 'delete revision'
   * operation, an account must also have access to 'delete' operation on an
   * entity.
   */
  protected const REVISION_OPERATION_MAP = [
    'view all revisions' => ['view', 'view'],
    'view revision' => ['view', 'view'],
    'revert revision' => ['revert', 'update'],
    'delete revision' => ['delete', 'delete'],
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a MediaAccessControlHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|null $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $entity_bundle = $entity->bundle();

    $etid = $entity->getEntityType()->id(); // box or biox

    /** @var \Drupal\box\Entity\BoxInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished box entities');
        }
        return AccessResult::allowedIfHasPermission($account, "view published {$etid} entities");

      case 'update':
        if ($account->hasPermission("edit any {$entity_bundle} {$etid}")) {
          return AccessResult::allowed();
        }
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowedIfHasPermission($account, "edit own {$entity_bundle} {$etid}");
        }
        return AccessResult::forbidden();

      case 'delete':
        if ($account->hasPermission("delete any {$entity_bundle} {$etid}")) {
          return AccessResult::allowed();
        }
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowedIfHasPermission($account, "delete own {$entity_bundle} {$etid}");
        }
        return AccessResult::forbidden();
    }

    // @todo: finish revision access.
    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $etid = $context['entity_type_id']; // box or biox

    return AccessResult::allowedIfHasPermission($account, "create {$entity_bundle} {$etid}");
  }


}
