<?php

namespace Drupal\box;

use Drupal\box\Entity\BoxType;
use Drupal\box\Entity\BoxTypeInterface;
use Drupal\box\Entity\BioxType;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for boxes of different types.
 */
class BoxPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    // Store our dependency.
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BoxPermissions {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Returns an array of box type permissions.
   *
   * @return array
   *   The box type permissions.
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function boxTypePermissions(): array {
    $perms = [];
    // Generate box permissions for all box types.
    foreach (BoxType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }
    foreach (BioxType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of box permissions for a given box type.
   *
   * @param \Drupal\box\Entity\BoxTypeInterface $type
   *   The box type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(BoxTypeInterface $type): array {
    $type_id = $type->id();
    $bundle_of = $type->getEntityType()->getBundleOf();

    /** @var \Drupal\box\Entity\BoxTypeInterface $bundle_of_entity_type */
    $bundle_of_entity_type = $this->entityTypeManager->getDefinition($bundle_of);
    $entity_type_id = $bundle_of_entity_type->id();

    $type_params = [
      '%type_name' => $type->label(),
      '@entity_type_label' => 'box', //$bundle_of_entity_type->getEntityType()->getSingularLabel(),
      '@entity_type_label_plural' => 'boxes', //$bundle_of_entity_type->getEntityType()->getPluralLabel(),
    ];

    return [
      "create $type_id $entity_type_id" => [
        'title' => $this->t('%type_name: Create new @entity_type_label', $type_params),
      ],
      "edit own $type_id $entity_type_id" => [
        'title' => $this->t('%type_name: Edit own @entity_type_label', $type_params),
      ],
      "edit any $type_id $entity_type_id" => [
        'title' => $this->t('%type_name: Edit any @entity_type_label', $type_params),
      ],
      "delete own $type_id $entity_type_id" => [
        'title' => $this->t('%type_name: Delete own @entity_type_label', $type_params),
      ],
      "delete any $type_id $entity_type_id" => [
        'title' => $this->t('%type_name: Delete any @entity_type_label', $type_params),
      ],
      "view $type_id $entity_type_id revisions" => [
        'title' => $this->t('%type_name: View @entity_type_label revisions', $type_params),
      ],
      "revert $type_id $entity_type_id revisions" => [
        'title' => $this->t('%type_name: Revert @entity_type_label revisions', $type_params),
        'description' => t('Role requires permission <em>view revisions</em> and <em>edit rights</em> for @entity_type_label_plural in question, or <em>Administer @entity_type_label_plural</em>.', $type_params),
      ],
      "delete $type_id $entity_type_id revisions" => [
        'title' => $this->t('%type_name: Delete @$entity_type_id revisions', $type_params),
        'description' => $this->t('Role requires permission to <em>view revisions</em> and <em>delete rights</em> for @entity_type_label_plural in question, or <em>Administer @entity_type_label_plural</em>.', $type_params),
      ],
    ];
  }

}
