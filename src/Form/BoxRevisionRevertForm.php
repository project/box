<?php

namespace Drupal\box\Form;

use Drupal\box\BoxStorageInterface;
use Drupal\box\Entity\BoxInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for reverting a Box revision.
 *
 * @ingroup box
 */
class BoxRevisionRevertForm extends ConfirmFormBase {


  /**
   * The Box revision.
   *
   * @var \Drupal\box\Entity\BoxInterface
   */
  protected BoxInterface $revision;

  /**
   * The Box storage.
   *
   * @var \Drupal\box\BoxStorageInterface
   */
  protected \Drupal\box\BoxStorageInterface|EntityStorageInterface $boxStorage;

  /**
   * The Box type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $boxTypeStorage;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new BoxRevisionRevertForm.
   *
   * @param \Drupal\box\BoxStorageInterface $box_storage
   *   The Box storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $box_type_storage
   *   The Box type storage.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(BoxStorageInterface $box_storage, EntityStorageInterface $box_type_storage, DateFormatterInterface $date_formatter, TimeInterface $time) {
    $this->boxStorage = $box_storage;
    $this->boxTypeStorage = $box_type_storage;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BoxRevisionRevertForm {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('box'),
      $entity_manager->getStorage('box_type'),
      $container->get('date.formatter'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'box_revision_revert_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to revert to the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.box.version_history', ['box' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Revert');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return new TranslatableMarkup('');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $box_revision = NULL): array {
    $this->revision = $this->boxStorage->loadRevision($box_revision);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // The revision timestamp will be updated when the revision is saved. Keep
    // the original one for the confirmation message.
    $original_revision_timestamp = $this->revision->getRevisionCreationTime();

    $this->revision = $this->prepareRevertedRevision($this->revision, $form_state);
    $this->revision->setRevisionLogMessage($this->t('Copy of the revision from %date.', ['%date' => $this->dateFormatter->format($original_revision_timestamp)]));
    $this->revision->setRevisionUserId($this->currentUser()->id());
    $this->revision->setRevisionCreationTime($this->time->getRequestTime());
    $this->revision->setChangedTime($this->time->getRequestTime());
    $this->revision->save();

    $this->logger('box')->notice('@type: reverted %title revision %revision.', [
      '@type' => $this->revision->bundle(),
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()
      ->addStatus($this->t('@type %title has been reverted to the revision from %revision-date.', [
        '%revision-date' => $this->dateFormatter->format($original_revision_timestamp),
        '@type' => $this->boxTypeStorage->load($this->revision->bundle())->label(),
        '%title' => $this->revision->label(),
      ]));

    $form_state->setRedirect('entity.box.version_history', ['box' => $this->revision->id()]);
  }

  /**
   * Prepares a revision to be reverted.
   *
   * @param \Drupal\box\Entity\BoxInterface $revision
   *   The revision to be reverted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\box\Entity\BoxInterface
   *   The prepared revision ready to be stored.
   */
  protected function prepareRevertedRevision(BoxInterface $revision, FormStateInterface $form_state): BoxInterface {
    $revision->setNewRevision();
    $revision->isDefaultRevision(TRUE);

    return $revision;
  }

}
