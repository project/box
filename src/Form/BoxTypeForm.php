<?php

namespace Drupal\box\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for box type forms.
 *
 * @package Drupal\box\Form
 */
class BoxTypeForm extends BundleEntityFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs the BoxTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BoxTypeForm {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\box\Entity\BoxInterface $box */
    /** @var \Drupal\box\Entity\BoxTypeInterface $box_type */
    $box_type = $this->entity;

    $bundle_of = $box_type->getEntityType()->getBundleOf();

    /** @var \Drupal\box\Entity\BoxTypeInterface $bundle_of_entity_type */
    $bundle_of_entity_type = $this->entityTypeManager->getDefinition($bundle_of);
    $etid = $bundle_of_entity_type->id();

    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add box type');
      $fields = $this->entityFieldManager->getBaseFieldDefinitions($etid);
    }
    else {
      $form['#title'] = $this->t('Edit %label box type', ['%label' => $box_type->label()]);
      $fields = $this->entityFieldManager->getFieldDefinitions($etid, $box_type->id());
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $box_type->label(),
      '#description' => $this->t('Label for the Box type. This label will be displayed on the <em>Add box</em> page.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $box_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ($etid == 'box') ? ['\Drupal\box\Entity\BoxType', 'load'] : ['\Drupal\box\Entity\BioxType', 'load'],
        'source' => ['label'],
      ],
      '#disabled' => $box_type->isLocked(),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $box_type->getDescription(),
      '#description' => $this->t('This text will be displayed on the <em>Add box</em> page.'),
    ];

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['submission'] = [
      '#type' => 'details',
      '#title' => $this->t('Submission form settings'),
      '#group' => 'additional_settings',
      '#open' => TRUE,
    ];
    $form['submission']['box_label_field_label'] = [
      '#title' => $this->t('Title field label'),
      '#type' => 'textfield',
      '#default_value' => $fields['label']->getLabel(),
      '#required' => TRUE,
    ];
    $form['submission']['box_label_field_description'] = [
      '#title' => $this->t('Title field description'),
      '#type' => 'textfield',
      '#default_value' => $fields['label']->getDescription(),
    ];
//    $form['submission']['box_id_field_label'] = [
//      '#title' => $this->t('ID field label'),
//      '#type' => 'textfield',
//      '#default_value' => $fields['id']->getLabel(),
//      '#required' => TRUE,
//    ];
//    $form['submission']['box_id_field_description'] = [
//      '#title' => $this->t('ID field description'),
//      '#type' => 'textfield',
//      '#default_value' => $fields['id']->getDescription(),
//    ];
    $form['workflow'] = [
      '#type' => 'details',
      '#title' => $this->t('Publishing options'),
      '#group' => 'additional_settings',
    ];
    $workflow_options = [
      //'status' => $box->status->value,
      'revision' => $box_type->shouldCreateNewRevision(),
      'require_log' => $box_type->isRevisionLogRequired(),
    ];
    // Prepare workflow options to be used for 'checkboxes' form element.
    $keys = array_keys(array_filter($workflow_options));
    $workflow_options = array_combine($keys, $keys);
    $form['workflow']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default options'),
      '#default_value' => $workflow_options,
      '#options' => [
        // 'status' => $this->t('Published'),
        'revision' => $this->t('Create new revision'),
        'require_log' => $this->t('Require revision log message'),
      ],
    ];
    /*if ($this->moduleHandler->moduleExists('language')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];

      $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle('box', $box_type->id());
      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'box',
          'bundle' => $box_type->id(),
        ],
        '#default_value' => $language_configuration,
      ];
    }*/

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save box type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    /** @var \Drupal\box\Entity\BoxTypeInterface $box_type */
    $box_type = $this->entity;

    $bundle_of = $box_type->getEntityType()->getBundleOf();

    /** @var \Drupal\box\Entity\BoxTypeInterface $bundle_of_entity_type */
    $bundle_of_entity_type = $this->entityTypeManager->getDefinition($bundle_of);
    $etid = $bundle_of_entity_type->id();

    $box_type->setNewRevision($form_state->getValue(['options', 'revision']));
    if ($form_state->getValue(['options', 'require_log'])) {
      $box_type->setRevisionLogRequired();
    }
    else {
      $box_type->setRevisionLogOptional();
    }

    $status = $box_type->save();

    $args = ['%title' => $box_type->label()];

    switch ($status) {
      case SAVED_NEW:
        $this->logger('box')
          ->notice('The box type %title has been created.', $args);
        $this->messenger()
          ->addStatus($this->t('The box type %title has been created.', $args));
        break;

      default:
        $this->messenger()
          ->addStatus($this->t('The box type %title has been updated.', $args));
    }

    $fields = $this->entityFieldManager->getFieldDefinitions($etid, $box_type->id());

    // Update basic field labels and descriptions.
    $label_field = $fields['label'];
    $id_field = $fields['id'];

    $box_label_field_label = $form_state->getValue('box_label_field_label');
    $box_label_field_description = $form_state->getValue('box_label_field_description');
    $box_id_field_label = $form_state->getValue('box_id_field_label');
    $box_id_field_description = $form_state->getValue('box_id_field_description');
    if ($label_field->getLabel() != $box_label_field_label) {
      $label_field->getConfig($box_type->id())->setLabel($box_label_field_label)->save();
    }
    if ($label_field->getLabel() != $box_label_field_description) {
      $label_field->getConfig($box_type->id())->setDescription($box_label_field_description)->save();
    }
    // @todo: https://www.drupal.org/node/3264267
//    if ($id_field->getLabel() != $box_id_field_label) {
//      $id_field->getConfig($box_type->id())->setLabel($box_id_field_label)->save();
//    }
//    if ($id_field->getLabel() != $box_id_field_description) {
//      $id_field->getConfig($box_type->id())->setDescription($box_id_field_description)->save();
//    }

    $this->entityFieldManager->clearCachedFieldDefinitions();

    $form_state->setRedirectUrl($box_type->toUrl('collection'));
  }

}
