<?php

namespace Drupal\box\Form;

use Drupal\box\BoxStorageInterface;
use Drupal\box\Entity\BoxInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Box revision.
 *
 * @ingroup box
 */
class BoxRevisionDeleteForm extends ConfirmFormBase {


  /**
   * The Box revision.
   *
   * @var \Drupal\box\Entity\BoxInterface
   */
  protected BoxInterface $revision;

  /**
   * The Box storage.
   *
   * @var \Drupal\box\BoxStorageInterface
   */
  protected BoxStorageInterface $boxStorage;

  /**
   * The Box type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $boxTypeStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a new BoxRevisionDeleteForm.
   *
   * @param \Drupal\box\BoxStorageInterface $box_storage
   *   The Box storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $box_type_storage
   *   The Box type storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(BoxStorageInterface $box_storage, EntityStorageInterface $box_type_storage, Connection $connection, DateFormatterInterface $date_formatter) {
    $this->boxStorage = $box_storage;
    $this->boxTypeStorage = $box_type_storage;
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BoxRevisionDeleteForm {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('box'),
      $entity_manager->getStorage('box_type'),
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'box_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.box.version_history', ['box' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $box_revision = NULL): array {
    $this->revision = $this->boxStorage->loadRevision($box_revision);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->boxStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('box')->notice('@type: deleted %title revision %revision.', [
      '@type' => $this->revision->bundle(),
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()
      ->addStatus($this->t('Revision from %revision-date of @type %title has been deleted.', [
        '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
        '@type' => $this->boxTypeStorage->load($this->revision->bundle())->label(),
        '%title' => $this->revision->label(),
      ]));
    $form_state->setRedirect('entity.box.canonical', ['box' => $this->revision->id()]);
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {box_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      // @todo
      // $form_state->setRedirect('entity.box.version_history', ['box' => $this->revision->id()]);
    }
  }

}
