<?php

namespace Drupal\box\Form;

use Drupal\box\Entity\BioxType;
use Drupal\box\Entity\BoxType;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Box edit forms.
 *
 * @ingroup box
 */
class BoxForm extends ContentEntityForm {

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a NodeForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, AccountInterface $current_user, DateFormatterInterface $date_formatter, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
    );
  }

  /**
   * Insert a value or key/value pair after a specific key in an array.  If key doesn't exist, value is appended
   * to the end of the array.
   *
   * @param array $array
   * @param string $key
   * @param array $new
   *
   * @return array
   */
  private function arrayInsertAfter( array $array, $key, array $new ) {
    $keys = array_keys( $array );
    $index = array_search( $key, $keys );
    $pos = false === $index ? count( $array ) : $index + 1;

    return array_merge( array_slice( $array, 0, $pos ), $new, array_slice( $array, $pos ) );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\box\Entity\BoxInterface $box */
    $box = $this->entity;

    $form = parent::buildForm($form, $form_state);

    $entity_type_id = $box->getEntityTypeId();

    if ($entity_type_id == 'box') {
      // The machine name field must be always directly after the source field.
      // This is not guaranteed by Drupal core.
      $id_field = $form['id'];
      $id_field['#weight'] = $form['label']['#weight'];
      unset($form['id']);
      $form = $this->arrayInsertAfter($form, 'label', ['id' => $id_field]);
    }

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit @type: @label', [
        '@type' => $box->bundleLabel(),
        '@label' => $box->label()
      ]);
    }

    $form['advanced'] = [
      '#type' => 'container',
      '#weight' => 99,
      '#attributes' => [
        'class' => ['box-form-advanced', 'entity-meta'],
      ],
    ];

    $form['meta'] = [
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -10,
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $box->isPublished() ? $this->t('Published') : $this->t('Not published'),
      '#access' => !$box->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$box->isNew() ? $this->dateFormatter->format($box->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved', 'container-inline']],
    ];
    $form['meta']['author'] = [
      '#type' => 'item',
      '#title' => $this->t('Author'),
      '#markup' => $box->getOwner()->getAccountName(),
      '#wrapper_attributes' => ['class' => ['entity-meta__author', 'container-inline']],
    ];

    $form['revision_information']['#type'] = 'container';
    $form['revision_information']['#group'] = 'meta';

    /** @var \Drupal\box\Entity\BoxTypeInterface $box_type */
    $box_type = ($entity_type_id == 'box') ? BoxType::load($box->bundle()) : BioxType::load($box->bundle());;
    if ($box_type->isRevisionLogRequired()) {
      /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type */
      $entity_type = $box->getEntityType();
      $override_revision_settings = $box->get($entity_type->getKey('revision'))->access('update');
      if (!$override_revision_settings) {
        // Get log message field's key from definition.
        $log_message_field = $entity_type->getRevisionMetadataKey('revision_log_message');
        if ($log_message_field && isset($form[$log_message_field])) {
          $form[$log_message_field]['widget'][0]['value']['#required'] = TRUE;
          unset($form[$log_message_field]['#states']);
          $form['revision_information']['#access'] = TRUE;
        }
      }
    }

    $form['#theme'] = ['box_edit_form'];
    $form['#attached']['library'][] = 'box/drupal.box.form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    /** @var \Drupal\box\Entity\BoxInterface $box */
    $box = &$this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('revision') && $form_state->getValue('revision')) {
      $box->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $box->setRevisionCreationTime($this->time->getRequestTime());
      $box->setRevisionUserId($this->currentUser->id());
    }
    else {
      $box->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    $args = ['@type' => $box->bundleLabel(), '%title' => $box->label()];
    $link = ['link' => $box->toLink($this->t('View'), 'canonical', ['box' => $box->id()])->toString()];

    switch ($status) {
      case SAVED_NEW:
        $this->logger('box')->notice('@type: created %title.', $args + $link);
        $this->messenger()->addStatus($this->t('@type box %title has been created.', $args));
        break;

      default:
        $this->logger('box')->notice('@type: updated %title.', $args + $link);
        $this->messenger()->addStatus($this->t('@type box %title has been updated.', $args));
    }

    $etid = $box->getEntityTypeId();
    $form_state->setRedirect("entity.{$etid}.collection", [$etid => $box->id()]);
    return $status;
  }

}
