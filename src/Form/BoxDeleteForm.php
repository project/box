<?php

namespace Drupal\box\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Box entities.
 *
 * @ingroup box
 */
class BoxDeleteForm extends ContentEntityDeleteForm {


}
