<?php

namespace Drupal\box\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm as EntityDeleteMultipleForm;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\Url;

/**
 * Provides a box deletion confirmation form.
 *
 * @internal
 */
class BoxDeleteMultiple extends EntityDeleteMultipleForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletedMessage($count): PluralTranslatableMarkup {
    return $this->formatPlural($count, 'Deleted @count box.', 'Deleted @count boxes.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getInaccessibleMessage($count): PluralTranslatableMarkup {
    return $this->formatPlural($count, "@count box has not been deleted because you do not have the necessary permissions.", "@count boxes have not been deleted because you do not have the necessary permissions.");
  }

}
