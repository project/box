<?php

namespace Drupal\box\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a biox operations bulk form element.
 *
 * @ViewsField("biox_bulk_form")
 */
class BioxBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No biox selected.');
  }

}
