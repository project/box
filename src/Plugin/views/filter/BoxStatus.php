<?php

namespace Drupal\box\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter by published status.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("box_status")
 */
class BoxStatus extends FilterPluginBase {

  public function adminSummary() {
  }

  protected function operatorForm(&$form, FormStateInterface $form_state) {
  }

  public function canExpose(): bool {
    return FALSE;
  }

  public function query() {
    $table = $this->ensureMyTable();
    $snippet = "$table.status = 1 OR ($table.uid = ***CURRENT_USER*** AND ***CURRENT_USER***)";
    $this->query->addWhereExpression($this->options['group'], $snippet);
  }

}
