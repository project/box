<?php

namespace Drupal\box\Plugin\views\filter;

use Drupal\user\Plugin\views\filter\Name;
use Drupal\views\Annotation\ViewsFilter;

/**
 * Filter handler to check for revisions a certain user has created.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("box_uid_revision")
 */
class UidRevision extends Name {

  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    $placeholder = $this->placeholder() . '[]';

    $args = array_values($this->value);

    $this->query->addWhereExpression($this->options['group'], "$this->tableAlias.uid IN($placeholder) OR
      ((SELECT COUNT(DISTINCT vid) FROM {box_revision} br WHERE br.revision_user IN ($placeholder) AND br.id = $this->tableAlias.id) > 0)", [$placeholder => $args],
      $args);
  }

}
