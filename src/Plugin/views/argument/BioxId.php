<?php

namespace Drupal\box\Plugin\views\argument;

use Drupal\box\BoxStorageInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler to accept a biox id.
 *
 * @ViewsArgument("biox_id")
 */
class BioId extends NumericArgument {

  /**
   * The biox storage.
   *
   * @var \Drupal\box\BoxStorageInterface
   */
  protected $bioxStorage;

  /**
   * Constructs the Id object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\box\BoxStorageInterface $biox_storage
   *   The box storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BoxStorageInterface $biox_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->bioxStorage = $biox_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')->getStorage('biox')
    );
  }

  /**
   * Override the behavior of title(). Get the title of the biox.
   */
  public function titleQuery(): array {
    $titles = [];

    $bioxes = $this->bioxStorage->loadMultiple($this->value);
    foreach ($bioxes as $biox) {
      $titles[] = $biox->label();
    }
    return $titles;
  }

}
