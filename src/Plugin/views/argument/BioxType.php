<?php

namespace Drupal\box\Plugin\views\argument;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\views\Plugin\views\argument\StringArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler to accept a biox type.
 *
 * @ViewsArgument("biox_type")
 */
class BioxType extends StringArgument {

  /**
   * BioxType storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $bioxTypeStorage;

  /**
   * Constructs a new Biox Type object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $biox_type_storage
   *   The entity storage class.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $biox_type_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->bioxTypeStorage = $biox_type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_manager->getStorage('biox_type')
    );
  }

  /**
   * Override the behavior of summaryName(). Get the user friendly version of the biox type.
   */
  public function summaryName($data) {
    return $this->bioxType($data->{$this->name_alias});
  }

  /**
   * Override the behavior of title(). Get the user friendly version of the biox type.
   */
  public function title() {
    return $this->bioxType($this->argument);
  }

  public function bioxType($type_name) {
    $type = $this->bioxTypeStorage->load($type_name);
    return $type ? $type->label() : $this->t('Unknown biox type');
  }

}
