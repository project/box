<?php

namespace Drupal\box\Plugin\views\argument;

use Drupal\box\BoxStorageInterface;
use Drupal\Core\Database\Connection;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler to accept a box revision id.
 *
 * @ViewsArgument("biox_vid")
 */
class BioxVid extends NumericArgument {

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The biox storage.
   *
   * @var \Drupal\box\BoxStorageInterface
   */
  protected $bioxStorage;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Database Service Object.
   * @param \Drupal\box\BoxStorageInterface $biox_storage
   *   The biox storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, BoxStorageInterface $biox_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->database = $database;
    $this->bioxStorage = $biox_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity.manager')->getStorage('biox')
    );
  }

  /**
   * Override the behavior of title(). Get the title of the revision.
   */
  public function titleQuery() {
    $titles = [];

    $results = $this->bioxStorage->getAggregateQuery()
      ->accessCheck(FALSE)
      ->allRevisions()
      ->groupBy('label')
      ->condition('vid', $this->value, 'IN')
      ->execute();

    foreach ($results as $result) {
      $titles[] = $result['label'];
    }

    return $titles;

  }

}
