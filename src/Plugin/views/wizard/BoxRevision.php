<?php

namespace Drupal\box\Plugin\views\wizard;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Tests creating box revision views with the wizard.
 *
 * @ViewsWizard(
 *   id = "box_revision",
 *   base_table = "box_field_revision",
 *   title = @Translation("Box revisions")
 * )
 */
class BoxRevision extends WizardPluginBase {

  /**
   * Set the created column.
   *
   * @var string
   */
  protected $createdColumn = 'changed';

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplayOptions(): array {
    $display_options = parent::defaultDisplayOptions();

    // Add permission-based access control.
    $display_options['access']['type'] = 'perm';
    $display_options['access']['options']['perm'] = 'view all revisions';

    // Remove the default fields, since we are customizing them here.
    unset($display_options['fields']);

    /* Field: Content revision: Created date */
    $display_options['fields']['changed']['id'] = 'changed';
    $display_options['fields']['changed']['table'] = 'box_field_revision';
    $display_options['fields']['changed']['field'] = 'changed';
    $display_options['fields']['changed']['entity_type'] = 'box';
    $display_options['fields']['changed']['entity_field'] = 'changed';
    $display_options['fields']['changed']['alter']['alter_text'] = FALSE;
    $display_options['fields']['changed']['alter']['make_link'] = FALSE;
    $display_options['fields']['changed']['alter']['absolute'] = FALSE;
    $display_options['fields']['changed']['alter']['trim'] = FALSE;
    $display_options['fields']['changed']['alter']['word_boundary'] = FALSE;
    $display_options['fields']['changed']['alter']['ellipsis'] = FALSE;
    $display_options['fields']['changed']['alter']['strip_tags'] = FALSE;
    $display_options['fields']['changed']['alter']['html'] = FALSE;
    $display_options['fields']['changed']['hide_empty'] = FALSE;
    $display_options['fields']['changed']['empty_zero'] = FALSE;
    $display_options['fields']['changed']['plugin_id'] = 'field';
    $display_options['fields']['changed']['type'] = 'timestamp';
    $display_options['fields']['changed']['settings']['date_format'] = 'medium';
    $display_options['fields']['changed']['settings']['custom_date_format'] = '';
    $display_options['fields']['changed']['settings']['timezone'] = '';

    /* Field: Content revision: Label */
    $display_options['fields']['label']['id'] = 'label';
    $display_options['fields']['label']['table'] = 'box_field_revision';
    $display_options['fields']['label']['field'] = 'label';
    $display_options['fields']['label']['entity_type'] = 'box';
    $display_options['fields']['label']['entity_field'] = 'label';
    $display_options['fields']['label']['label'] = '';
    $display_options['fields']['label']['alter']['alter_text'] = 0;
    $display_options['fields']['label']['alter']['make_link'] = 0;
    $display_options['fields']['label']['alter']['absolute'] = 0;
    $display_options['fields']['label']['alter']['trim'] = 0;
    $display_options['fields']['label']['alter']['word_boundary'] = 0;
    $display_options['fields']['label']['alter']['ellipsis'] = 0;
    $display_options['fields']['label']['alter']['strip_tags'] = 0;
    $display_options['fields']['label']['alter']['html'] = 0;
    $display_options['fields']['label']['hide_empty'] = 0;
    $display_options['fields']['label']['empty_zero'] = 0;
    $display_options['fields']['label']['settings']['link_to_entity'] = 0;
    $display_options['fields']['label']['plugin_id'] = 'field';
    return $display_options;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplayFiltersUser(array $form, FormStateInterface $form_state): array {
    $filters = [];

    $type = $form_state->getValue(['show', 'type']);
    if ($type && $type != 'all') {
      $filters['type'] = [
        'id' => 'type',
        'table' => 'box_field_data',
        'field' => 'type',
        'relationship' => 'box_id',
        'value' => [$type => $type],
        'entity_type' => 'box',
        'entity_field' => 'type',
        'plugin_id' => 'bundle',
      ];
    }
    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildDisplayOptions($form, FormStateInterface $form_state) {
    $display_options = parent::buildDisplayOptions($form, $form_state);
    if (isset($display_options['default']['filters']['type'])) {
      $display_options['default']['relationships']['box_id'] = [
        'id' => 'box_id',
        'table' => 'box_field_revision',
        'field' => 'box_id',
        'relationship' => 'none',
        'group_type' => 'group',
        'admin_label' => 'Get the actual box from a box revision.',
        'required' => 'true',
        'entity_type' => 'box',
        'entity_field' => 'box_id',
        'plugin_id' => 'standard',
      ];
    }
    return $display_options;
  }

}
