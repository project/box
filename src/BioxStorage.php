<?php

namespace Drupal\box;

use Drupal\box\Entity\BoxInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for Biox entities.
 *
 * This extends the base storage class, adding required special handling for
 * Biox entities.
 *
 * @ingroup box
 */
class BioxStorage extends SqlContentEntityStorage implements BoxStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(BoxInterface $entity): array {
    return $this->database->query(
      'SELECT vid FROM {biox_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account): array {
    return $this->database->query(
      'SELECT vid FROM {biox_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(BoxInterface $entity): int {
    return $this->database->query('SELECT COUNT(*) FROM {biox_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('biox_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
