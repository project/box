<?php

namespace Drupal\box\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Box entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class BoxRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($history_route = $this->getHistoryRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.version_history", $history_route);
    }

    if ($revision_route = $this->getRevisionRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision", $revision_route);
    }

    if ($revert_route = $this->getRevisionRevertRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision_revert", $revert_route);
    }

    if ($delete_route = $this->getRevisionDeleteRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.revision_delete", $delete_route);
    }

    if ($translation_route = $this->getRevisionTranslationRevertRoute($entity_type)) {
      $collection->add("{$entity_type_id}.revision_revert_translation_confirm", $translation_route);
    }

    return $collection;
  }

  /**
   * Helper function to set route requirements and options for revision routes.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to modify.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param string $op
   *   Entity operation
   *
   * @return void
   */
  private function extendRevisionRouteDefinition($route, EntityTypeInterface $entity_type, string $op, $main_route = FALSE) {
    $etid = $entity_type->id(); // box or biox

    $route->setRequirement('_entity_access', "{$etid}_revision.{$op}");
    // @todo
    //$route->setRequirement($etid, (($etid == 'box') ? '/^[a-z0-9_]+$/' : '\d+'));
    $route->setOption('_node_operation_route', TRUE);

    $parameters = [];
    $parameters[$etid] = ['type' => "entity:{$etid}"];
    if (!$main_route) {
      // The path contains the revision id also.
      $parameters["{$etid}_revision"] = ['type' => "entity_revision:{$etid}"];
    }

    $route->setOption('parameters', $parameters);
  }

  /**
   * Gets the version history route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getHistoryRoute(EntityTypeInterface $entity_type): ?Route {
    if (!$entity_type->hasLinkTemplate('version-history')) {
      return NULL;
    }

    $route = new Route($entity_type->getLinkTemplate('version-history'));
    $route->setDefaults([
      '_title' => "Revisions",
      '_controller' => '\Drupal\box\Controller\BoxController::revisionOverview',
    ]);

    $this->extendRevisionRouteDefinition($route, $entity_type, 'view all revisions', TRUE);
    return $route;
  }

  /**
   * Gets the revision route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionRoute(EntityTypeInterface $entity_type): ?Route {
    $entity_type_id = $entity_type->id();
    if (!$entity_type->hasLinkTemplate('revision')) {
      return NULL;
    }

    $route = new Route($entity_type->getLinkTemplate('revision'));
    $route->setDefaults([
      '_controller' => '\Drupal\box\Controller\BoxController::revisionShow',
      '_title_callback' => '\Drupal\box\Controller\BoxController::revisionPageTitle',
    ]);

    $this->extendRevisionRouteDefinition($route, $entity_type, 'view revision');
    return $route;
  }

  /**
   * Gets the revision revert route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionRevertRoute(EntityTypeInterface $entity_type): ?Route {
    if (!$entity_type->hasLinkTemplate('revision-revert')) {
      return NULL;
    }

    $route = new Route($entity_type->getLinkTemplate('revision-revert'));
    $route->setDefaults([
      '_form' => '\Drupal\box\Form\BoxRevisionRevertForm',
      '_title' => 'Revert to earlier revision',
    ]);

    $this->extendRevisionRouteDefinition($route, $entity_type, 'revert revision');
    return $route;
  }

  /**
   * Gets the revision delete route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionDeleteRoute(EntityTypeInterface $entity_type): ?Route {
    if (!$entity_type->hasLinkTemplate('revision-delete')) {
      return NULL;
    }

    $route = new Route($entity_type->getLinkTemplate('revision-delete'));
    $route->setDefaults([
      '_form' => '\Drupal\box\Form\BoxRevisionDeleteForm',
      '_title' => 'Delete earlier revision',
    ]);
    $this->extendRevisionRouteDefinition($route, $entity_type, 'delete revision');
    return $route;
  }

  /**
   * Gets the revision translation revert route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionTranslationRevertRoute(EntityTypeInterface $entity_type): ?Route {
    if (!$entity_type->hasLinkTemplate('translation-revert')) {
      return NULL;
    }

    $route = new Route($entity_type->getLinkTemplate('translation-revert'));
    $route->setDefaults([
      '_form' => '\Drupal\box\Form\BoxRevisionRevertTranslationForm',
      '_title' => 'Revert to earlier revision of a translation',
    ]);

    $this->extendRevisionRouteDefinition($route, $entity_type, 'revert revision');
    return $route;
  }

}
