<?php

namespace Drupal\box;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the biox entity type.
 */
class BioxViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['biox_field_data']['table']['base']['weight'] = -10;
    $data['biox_field_data']['table']['wizard_id'] = 'biox';

    $data['biox_field_data']['id']['argument'] = [
      'id' => 'biox_id',
      'name field' => 'label',
      'numeric' => FALSE,
      'validate type' => 'id',
    ];

    $data['biox_field_data']['label']['field']['default_formatter_settings'] = ['link_to_entity' => TRUE];

    $data['biox_field_data']['label']['field']['link_to_biox default'] = TRUE;

    $data['biox_field_data']['type']['argument']['id'] = 'biox_type';

    $data['biox_field_data']['langcode']['help'] = $this->t('The language of the biox or translation.');

    $data['biox_field_data']['status']['filter']['label'] = $this->t('Published status');
    $data['biox_field_data']['status']['filter']['type'] = 'yes-no';
    // Use status = 1 instead of status <> 0 in WHERE statement.
    $data['biox_field_data']['status']['filter']['use_equal'] = TRUE;

    $data['biox_field_data']['status_extra'] = [
      'title' => $this->t('Published status or admin user'),
      'help' => $this->t('Filters out unpublished bioxes if the current user cannot view it.'),
      'filter' => [
        'field' => 'status',
        'id' => 'biox_status',
        'label' => $this->t('Published status or admin user'),
      ],
    ];

    $data['biox_field_data']['biox_bulk_form'] = [
      'title' => $this->t('Biox operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple bioxes.'),
      'field' => [
        'id' => 'biox_bulk_form',
      ],
    ];

    // Bogus fields for aliasing purposes.
    // @todo Add similar support to any date field
    // @see https://www.drupal.org/node/2337507
    $data['biox_field_data']['created_fulldate'] = [
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_fulldate',
      ],
    ];

    $data['biox_field_data']['changed_fulldate'] = [
      'title' => $this->t('Updated date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_fulldate',
      ],
    ];

    $data['biox_field_data']['uid']['help'] = $this->t('The user authoring the biox. If you need more fields than the uid add the biox: author relationship');
    $data['biox_field_data']['uid']['filter']['id'] = 'user_name';
    $data['biox_field_data']['uid']['relationship']['title'] = $this->t('Biox author');
    $data['biox_field_data']['uid']['relationship']['help'] = $this->t('Relate biox to the user who created it.');
    $data['biox_field_data']['uid']['relationship']['label'] = $this->t('author');

    $data['biox_field_data']['uid_revision']['title'] = $this->t('User has a revision');
    $data['biox_field_data']['uid_revision']['help'] = $this->t('All bioxes where a certain user has a revision');
    $data['biox_field_data']['uid_revision']['real field'] = 'nid';
    $data['biox_field_data']['uid_revision']['filter']['id'] = 'biox_uid_revision';
    $data['biox_field_data']['uid_revision']['argument']['id'] = 'biox_uid_revision';

    $data['biox_field_revision']['table']['wizard_id'] = 'biox_revision';

    // Advertise this table as a possible base table.
    $data['biox_field_revision']['table']['base']['help'] = $this->t('Biox revision is a history of changes on biox.');
    $data['biox_field_revision']['table']['base']['defaults']['title'] = 'title';

    $data['biox_field_revision']['id']['argument'] = [
      'id' => 'biox_id',
      'numeric' => TRUE,
    ];

    $data['biox_field_revision']['id']['relationship']['id'] = 'standard';
    $data['biox_field_revision']['id']['relationship']['base'] = 'biox_field_data';
    $data['biox_field_revision']['id']['relationship']['base field'] = 'id';
    $data['biox_field_revision']['id']['relationship']['title'] = $this->t('Biox');
    $data['biox_field_revision']['id']['relationship']['label'] = $this->t('Get the actual biox from a biox revision.');
    $data['biox_field_revision']['id']['relationship']['extra'][] = [
      'field' => 'langcode',
      'left_field' => 'langcode',
    ];

    $data['biox_field_revision']['vid'] = [
      'argument' => [
        'id' => 'biox_vid',
        'numeric' => TRUE,
      ],
      'relationship' => [
        'id' => 'standard',
        'base' => 'biox_field_data',
        'base field' => 'vid',
        'title' => $this->t('Biox'),
        'label' => $this->t('Get the actual biox from a biox revision.'),
        'extra' => [
          [
            'field' => 'langcode',
            'left_field' => 'langcode',
          ],
        ],     ],
    ];/* + $data['biox_field_revision']['vid'];*/

    $data['biox_revision']['revision_user']['help'] = $this->t('Relate a biox revision to the user who created the revision.');
    $data['biox_revision']['revision_user']['relationship']['label'] = $this->t('revision user');

    $data['biox_field_revision']['table']['join']['biox_field_data']['left_field'] = 'vid';
    $data['biox_field_revision']['table']['join']['biox_field_data']['field'] = 'vid';

    $data['biox_field_revision']['status']['filter']['label'] = $this->t('Published');
    $data['biox_field_revision']['status']['filter']['type'] = 'yes-no';
    $data['biox_field_revision']['status']['filter']['use_equal'] = TRUE;

    $data['biox_field_revision']['langcode']['help'] = $this->t('The language of the biox or translation.');

    $data['biox_field_revision']['link_to_revision'] = [
      'field' => [
        'title' => $this->t('Link to revision'),
        'help' => $this->t('Provide a simple link to the revision.'),
        'id' => 'biox_revision_link',
        'click sortable' => FALSE,
      ],
    ];

    $data['biox_field_revision']['revert_revision'] = [
      'field' => [
        'title' => $this->t('Link to revert revision'),
        'help' => $this->t('Provide a simple link to revert to the revision.'),
        'id' => 'biox_revision_link_revert',
        'click sortable' => FALSE,
      ],
    ];

    $data['biox_field_revision']['delete_revision'] = [
      'field' => [
        'title' => $this->t('Link to delete revision'),
        'help' => $this->t('Provide a simple link to delete the content revision.'),
        'id' => 'biox_revision_link_delete',
        'click sortable' => FALSE,
      ],
    ];

    return $data;
  }

}
