<?php

namespace Drupal\box;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for box.
 */
class BoxTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
