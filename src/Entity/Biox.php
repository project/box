<?php

namespace Drupal\box\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Biox entity.
 *
 * @ingroup biox
 *
 * @ContentEntityType(
 *   id = "biox",
 *   label = @Translation("Biox"),
 *   label_collection = @Translation("Bioxes"),
 *   label_singular = @Translation("biox"),
 *   label_plural = @Translation("bioxes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count biox",
 *     plural = "@count bioxes"
 *   ),
 *   bundle_label = @Translation("Biox type"),
 *   handlers = {
 *     "storage" = "Drupal\box\BioxStorage",
 *     "storage_schema" = "Drupal\box\BioxStorageSchema",
 *     "access" = "Drupal\box\BoxAccessControlHandler",
 *     "view_builder" = "Drupal\box\BoxViewBuilder",
 *     "views_data" = "Drupal\box\BioxViewsData",
 *     "form" = {
 *       "default" = "Drupal\box\Form\BoxForm",
 *       "add" = "Drupal\box\Form\BoxForm",
 *       "edit" = "Drupal\box\Form\BoxForm",
 *       "delete" = "Drupal\box\Form\BoxDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\box\Form\BoxDeleteMultiple"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\box\Routing\BoxRouteProvider",
 *     },
 *     "list_builder" = "Drupal\box\BoxListBuilder",
 *     "translation" = "Drupal\box\BoxTranslationHandler",
 *   },
 *   base_table = "biox",
 *   data_table = "biox_field_data",
 *   revision_table = "biox_revision",
 *   revision_data_table = "biox_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "published" = "status",
 *     "status" = "status"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   bundle_entity_type = "biox_type",
 *   field_ui_base_route = "entity.biox_type.edit_form",
 *   permission_granularity = "bundle",
 *   common_reference_target = TRUE,
 *   admin_permission = "administer biox entities",
 *   permission_granularity = "bundle",
 *   links = {
 *     "canonical" = "/biox/{biox}",
 *     "add-page" = "/biox/add",
 *     "add-form" = "/biox/add/{biox_type}",
 *     "edit-form" = "/biox/{biox}/edit",
 *     "delete-form" = "/biox/{biox}/delete",
 *     "delete-multiple-form" = "/admin/content/biox/delete",
 *     "version-history" = "/biox/{biox}/revisions",
 *     "revision" = "/biox/{biox}/revisions/{biox_revision}/view",
 *     "revision-delete" = "/biox/{biox}/revisions/{biox_revision}/delete",
 *     "revision-revert" = "/biox/{biox}/revisions/{biox_revision}/revert",
 *     "translation-revert" = "/biox/{biox}/revisions/{box_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/biox",
 *   }
 * )
 */
class Biox extends Box implements BoxInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Set ID back to integer.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function bundleLabel(): ?string {
    $bundle = BioxType::load($this->bundle());
    return $bundle?->label();
  }

}
