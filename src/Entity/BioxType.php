<?php

namespace Drupal\box\Entity;

use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Defines the Biox bundle entity.
 *
 * @ConfigEntityType(
 *   id = "biox_type",
 *   label = @Translation("Biox type"),
 *   label_collection = @Translation("Biox types"),
 *   label_singular = @Translation("biox type"),
 *   label_plural = @Translation("biox types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count biox type",
 *     plural = "@count biox types"
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\box\BoxTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\box\Form\BoxTypeForm",
 *       "edit" = "Drupal\box\Form\BoxTypeForm",
 *       "delete" = "Drupal\box\Form\BoxTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "biox_type",
 *   admin_permission = "administer biox types",
 *   bundle_of = "biox",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/biox-types/add",
 *     "edit-form" = "/admin/structure/biox-types/manage/{biox_type}",
 *     "delete-form" = "/admin/structure/biox-types/manage/{biox_type}/delete",
 *     "collection" = "/admin/structure/biox-types"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "description",
 *     "new_revision",
 *     "require_revision_log"
 *   }
 * )
 */
class BioxType extends BoxType implements BoxTypeInterface {

}
