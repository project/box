<?php

namespace Drupal\box\Entity;

use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerTrait;
use stdClass;

/**
 * Defines the Box entity.
 *
 * @ingroup box
 *
 * @ContentEntityType(
 *   id = "box",
 *   label = @Translation("Box"),
 *   label_collection = @Translation("Boxes"),
 *   label_singular = @Translation("box"),
 *   label_plural = @Translation("boxes"),
 *   label_count = @PluralTranslation(
 *     singular = "@count box",
 *     plural = "@count boxes"
 *   ),
 *   bundle_label = @Translation("Box type"),
 *   handlers = {
 *     "storage" = "Drupal\box\BoxStorage",
 *     "storage_schema" = "Drupal\box\BoxStorageSchema",
 *     "access" = "Drupal\box\BoxAccessControlHandler",
 *     "view_builder" = "Drupal\box\BoxViewBuilder",
 *     "views_data" = "Drupal\box\BoxViewsData",
 *     "form" = {
 *       "default" = "Drupal\box\Form\BoxForm",
 *       "add" = "Drupal\box\Form\BoxForm",
 *       "edit" = "Drupal\box\Form\BoxForm",
 *       "delete" = "Drupal\box\Form\BoxDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\box\Form\BoxDeleteMultiple"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\box\Routing\BoxRouteProvider",
 *     },
 *     "list_builder" = "Drupal\box\BoxListBuilder",
 *     "translation" = "Drupal\box\BoxTranslationHandler",
 *   },
 *   base_table = "box",
 *   data_table = "box_field_data",
 *   revision_table = "box_revision",
 *   revision_data_table = "box_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "published" = "status",
 *     "status" = "status"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   bundle_entity_type = "box_type",
 *   field_ui_base_route = "entity.box_type.edit_form",
 *   permission_granularity = "bundle",
 *   common_reference_target = TRUE,
 *   admin_permission = "administer box entities",
 *   permission_granularity = "bundle",
 *   links = {
 *     "canonical" = "/box/{box}",
 *     "add-page" = "/box/add",
 *     "add-form" = "/box/add/{box_type}",
 *     "edit-form" = "/box/{box}/edit",
 *     "delete-form" = "/box/{box}/delete",
 *     "delete-multiple-form" = "/admin/content/box/delete",
 *     "version-history" = "/box/{box}/revisions",
 *     "revision" = "/box/{box}/revisions/{box_revision}/view",
 *     "revision-delete" = "/box/{box}/revisions/{box_revision}/delete",
 *     "revision-revert" = "/box/{box}/revisions/{box_revision}/revert",
 *     "translation-revert" = "/box/{box}/revisions/{box_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/box",
 *   }
 * )
 */
class Box extends EditorialContentEntityBase implements BoxInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    // This override exists to set the operation to the default value "view".
    return parent::access($operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values): void {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if (in_array($rel, ['revision-delete', 'revision-revert'])) {
      $uri_route_parameters['box_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the box owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSaveRevision(EntityStorageInterface $storage, stdClass $record) {
    parent::preSaveRevision($storage, $record);

    if (!$this->isNewRevision() && isset($this->original) && (!isset($record->revision_log) || $record->revision_log === '')) {
      // If we are updating an existing box without adding a new revision, we
      // need to make sure $entity->revision_log is reset whenever it is empty.
      // Therefore, this code allows us to avoid clobbering an existing log
      // entry with an empty one.
      $record->revision_log = $this->original->getRevisionLogMessage();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label): BoxInterface {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): BoxInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setSetting('max_length', 128)
      ->setRequired(TRUE)
      ->addConstraint('UniqueField', [])
      ->addPropertyConstraints('value', ['Regex' => ['pattern' => '/^[a-z0-9_]+$/']])
      ->setDisplayOptions('form', [
        'type' => 'machine_name',
        'settings' => [
          'weight' => 100,
          'source_field' => 'label',
          'disable_on_edit' => TRUE,
          'standalone' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Box entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Box is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function bundleLabel(): ?string {
    $bundle = BoxType::load($this->bundle());
    return $bundle?->label();
  }

}
