<?php

namespace Drupal\box\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Defines the Box bundle entity.
 *
 * @ConfigEntityType(
 *   id = "box_type",
 *   label = @Translation("Box type"),
 *   label_collection = @Translation("Box types"),
 *   label_singular = @Translation("box type"),
 *   label_plural = @Translation("box types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count box type",
 *     plural = "@count box types"
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\box\BoxTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\box\Form\BoxTypeForm",
 *       "edit" = "Drupal\box\Form\BoxTypeForm",
 *       "delete" = "Drupal\box\Form\BoxTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "box_type",
 *   admin_permission = "administer box types",
 *   bundle_of = "box",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/box-types/add",
 *     "edit-form" = "/admin/structure/box-types/manage/{box_type}",
 *     "delete-form" = "/admin/structure/box-types/manage/{box_type}/delete",
 *     "collection" = "/admin/structure/box-types"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "description",
 *     "new_revision",
 *     "require_revision_log"
 *   }
 * )
 */
class BoxType extends ConfigEntityBundleBase implements BoxTypeInterface {

  /**
   * The Box type ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Box type label.
   *
   * @var string
   */
  protected string $label;

  /**
   * A brief description of this Box type.
   *
   * @var string
   */
  protected string $description;

  /**
   * Default value of the 'Create new revision' checkbox of this box type.
   *
   * @var bool
   */
  protected bool $new_revision = TRUE;

  /**
   * Default value of the 'Require log message' checkbox of this box type.
   *
   * @var bool
   */
  protected bool $require_revision_log = FALSE;

  /**
   * {@inheritdoc}
   */
  public function isLocked(): bool {
    $locked = \Drupal::state()->get('box.type.locked');
    return !empty($locked[$this->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->description ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): ?string {
    return $this->id ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewRevision(bool $new_revision): void {
    $this->new_revision = $new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision(): bool {
    return $this->new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function isRevisionLogRequired(): bool {
    return $this->require_revision_log;
  }

  /**
   * {@inheritdoc}
   */
  public function setRevisionLogRequired(): void {
    $this->require_revision_log = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setRevisionLogOptional(): void {
    $this->require_revision_log = FALSE;
  }

}
