<?php

namespace Drupal\box\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Provides an interface for defining Box type entities.
 */
interface BoxTypeInterface extends ConfigEntityInterface, RevisionableEntityBundleInterface {

  /**
   * Determines whether the box type is locked.
   *
   * @return bool
   *   TRUE if the entity is locked, FALSE otherwise.
   */
  public function isLocked(): bool;

  /**
   * Gets the description.
   *
   * @return string|null
   *   The description of this box type.
   */
  public function getDescription(): ?string;

  /**
   * Sets whether a new revision should be created by default.
   *
   * @param bool $new_revision
   *   TRUE if a new revision should be created by default.
   */
  public function setNewRevision(bool $new_revision): void;

  /**
   * Checks whether the 'Revision log message' fields should be required.
   *
   * @return bool
   *   TRUE if the field should be required, FALSE otherwise.
   */
  public function isRevisionLogRequired(): bool;

  /**
   * Require 'Revision log message'.
   */
  public function setRevisionLogRequired(): void;

  /**
   * Do not require 'Revision log message'
   */
  public function setRevisionLogOptional(): void;

}
