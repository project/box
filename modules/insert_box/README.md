Enable "Insert boxes" text filter for desired text formats
Embed boxes using [box:id] or [box:id:view_mode] where id is a machine name.


Module description
------------------
Why would you need a new module when custom block types seem to be ideally
poised to accomplish this task?
You can indeed use Insert Block module, and it may work just fine.
However, using blocks has three major drawbacks which might or might not affect
you depending on your use case.
 1. If you wish to use custom blocks with Insert Block module, it is possible
   with #2844631: Use Custom Blocks (Block Content), but discouraged in #
   #2704331-19: Ability to display block_content entities independently, also
   outside of Blocks, since custom blocks are not intended to be rendered on
   their own in Drupal 8 and this functionality has been postponed as feature
   request for Drupal 9. So approach used in #2844631: Use Custom Blocks
   cannot be guaranteed to work seamlessly with future Drupal 8.x releases.
 2. If you stick with standard blocks, they need to be placed in some region
   before you get block's delta and are able to embed it. If you want to just
   embed the block, this adds unnecessary step to the process, and you end up
   with a block being placed in a region but never displayed, which harms
   admin UX. As the number of blocks grows, you might end up with block
   overview page, which is very crowded and difficult to navigate.
 3. Blocks still come with a limited set of permissions, though that should
   improve soon. See #1975064: Add more granular block permissions
