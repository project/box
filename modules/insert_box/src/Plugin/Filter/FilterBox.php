<?php

namespace Drupal\insert_box\Plugin\Filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\filter\Annotation\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FilterBox
 *
 * Inserts boxes into the content.
 *
 * @package Drupal\box\Plugin\Filter
 *
 * @Filter(
 *   id = "filter_box",
 *   title = @Translation("Insert boxes"),
 *   description = @Translation("Inserts boxes into content using [box:entity_id] tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *     "check_roles" = TRUE
 *   }
 * )
 */
class FilterBox extends FilterBase implements ContainerFactoryPluginInterface {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @var \Drupal\Core\Render\Renderer $renderer
   */
  protected EntityTypeManagerInterface $entityTypeManager;
  protected Renderer $renderer;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Render\Renderer
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, Renderer $renderer) {
    // Call parent construct method.
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Store our dependency.
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): FilterBox {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {

    if (preg_match_all("/\[box:([a-z0-9_]+)(:[^\]]+|)\]/", $text, $match)) {
      /** @var \Drupal\box\BioxStorage $box_storage */
      $box_storage =  $this->entityTypeManager->getStorage('box');
      // Generate replacements from box:id pattern.
      $raw_tags = $replacements = [];
      foreach ($match[1] as $key => $value) {
        $raw_tags[] = $match[0][$key];
        $box_id = $match[1][$key];
        $view_mode = $match[2][$key];

        if ($box = $box_storage->load($box_id)) {
          $replacements[] = $this->generateReplacement($box, $view_mode);
        }
      }
      $text = str_replace($raw_tags, $replacements, $text);
    }

    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE): string {
    return $this->t('Please use [box:<em>box_entity_id</em>] and [box:<em>box_entity_id</em>:<em>box_view_mode</em>] tags to display the box.');
  }

  /**
   * Renders box to be used as replacement.
   *
   * @param \Drupal\box\Entity\Box $box
   * @param string $view_mode
   *
   * @return \Drupal\Component\Render\MarkupInterface
   */
  private function generateReplacement($box, $view_mode) {
    if (!$box->access('view')) {
      return '';
    }

    // @todo Check whether view mode exists
    if (!empty($view_mode)) {
      // Remove leading colon.
      $view_mode = substr($view_mode, 1);

      $box_view = $this->entityTypeManager->getViewBuilder('box')->view($box, $view_mode);
    }
    else {
      $box_view = $this->entityTypeManager->getViewBuilder('box')->view($box);
    }
    return $this->renderer->render($box_view);
  }

}
