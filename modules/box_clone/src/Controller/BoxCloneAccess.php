<?php

namespace Drupal\box_clone\Controller;

use Drupal\box\Entity\Box;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

class BoxCloneAccess {

  /**
   * Limit access to the clone according to their restricted state.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\box\Entity\Box $box
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function cloneBox(AccountInterface $account, Box $box) {
    $box = Box::load($box);

    if (_box_clone_has_clone_permission($box)) {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden();
    }

    $result->addCacheableDependency($box);

    return $result;
  }

}
