<?php

namespace Drupal\box_clone\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Handler for showing box clone link.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("box_clone_link")
 */
class CloneLink extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['text'] = ['default' => $this->getDefaultLabel()];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to display'),
      '#default_value' => $this->options['text'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {}

  /**
   * Returns the default label for the link.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The default link label.
   */
  protected function getDefaultLabel(): TranslatableMarkup {
    return $this->t('Duplicate');
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $box = $this->getEntity($values);

    if (!$box) {
      return '';
    }

    $entity_type_id = $box->getEntityTypeId();
    $url = Url::fromRoute("box_clone.{$entity_type_id}.clone", [
      'box' => $box->id(),
    ]);

    if (!$url->access()) {
      return '';
    }

    return [
      '#type' => 'link',
      '#url' => $url,
      '#title' => $this->options['text'] ?: $this->getDefaultLabel(),
    ];
  }

}
