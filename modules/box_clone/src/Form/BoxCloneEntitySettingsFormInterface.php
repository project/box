<?php

namespace Drupal\box_clone\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an interface for an Clone Entity forms.
 */
interface BoxCloneEntitySettingsFormInterface {

  /**
   * Sets the entity type the settings form is for.
   *
   * @param string $entityTypeId
   *   The entity type id i.e. article.
   */
  public function setEntityType(string $entityTypeId);

  /**
   * Returns the entity type Id. i.e. article.
   *
   * @return string
   *   The entity type id.
   */
  public function getEntityTypeId(): string;

  /**
   * The array of config names.
   *
   * @return array
   *   The config.
   */
  public function getEditableConfigNames(): array;

  /**
   * Returns the bundles for the entity.
   *
   * @return array
   *   The bundles of the entity.
   */
  public function getEntityBundles(): array;

  /**
   * Returns the selected bundles on the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array|mixed|null
   *   An array/mixed set of content types if there are any, or null.
   */
  public function getSelectedBundles(FormStateInterface $form_state);

  /**
   * Returns the description field.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return string
   *   The description text.
   */
  public function getDescription(FormStateInterface $form_state): string;

  /**
   * Returns the default fields.
   *
   * @param string $value
   *   The bundle name.
   *
   * @return array
   *   The default fields.
   */
  public function getDefaultFields(string $value): array;

  /**
   * Return the configuration.
   *
   * @param string $value
   *   The setting name.
   *
   * @return array|mixed|null
   *   Returns the setting value if it exists, or NULL.
   */
  public function getSettings(string $value);

}
