<?php

namespace Drupal\box_clone\Form;

/**
 * Module settings form.
 */
class BoxCloneParagraphSettingsForm extends BoxCloneEntitySettingsForm {

  /**
   * The machine name of the entity type.
   *
   * @var string
   *   The entity type id i.e. node
   */
  protected $entityTypeId = 'paragraph';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'box_clone_paragraph_setting_form';
  }

}
