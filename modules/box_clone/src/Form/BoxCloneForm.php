<?php

namespace Drupal\box_clone\Form;

use Drupal\box\Form\BoxForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for clone box edit form.
 */
class BoxCloneForm extends BoxForm {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);

    $element['submit']['#access'] = TRUE;
    $element['submit']['#value'] = $this->t('Duplicate');

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    /** @var \Drupal\box\Entity\BoxInterface $box */
    $box = $this->entity;
    $insert = $box->isNew();
    $status = $box->save();
    $box_link = $box->toLink($this->t('View'))->toString();
    $context = [
      '@type' => $box->bundle(),
      '%title' => $box->label(),
      'link' => $box_link,
    ];
    $t_args = [
      '@type' => $box->bundleLabel(),
      '%title' => $box->label(),
    ];

    if ($insert) {
      $this->logger('box')
        ->notice('@type: added %title (clone).', $context);
      $this->messenger()->addMessage($this->t('@type %title (clone) has been created.', $t_args));
    }

    if ($box->id()) {
      $form_state->setValue('id', $box->id());
      $form_state->set('id', $box->id());
      $storage = $form_state->getStorage();
      if ($box->access('view')) {
        $form_state->setRedirect(
          'entity.box.canonical',
          ['box' => $box->id()]
        );
      }
      else {
        $form_state->setRedirect('<front>');
      }

    }
    else {
      // In the unlikely case something went wrong on save, the box will be
      // rebuilt and box form redisplayed the same way as in preview.
      $this->messenger()->addError($this->t('The cloned box could not be saved.'));
      $form_state->setRebuild();
    }

    return $status;
  }

}
