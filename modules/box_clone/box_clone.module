<?php

use Drupal\box\Entity\Box;
use Drupal\box\Entity\BoxInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Implements hook_entity_type_build().
 */
function box_clone_entity_type_build(array &$entity_types) {
  if (isset($entity_types['box'])) {
    $entity_types['box']->setFormClass('box_clone', 'Drupal\box_clone\Form\BoxCloneForm');
  }
  if (isset($entity_types['biox'])) {
    $entity_types['biox']->setFormClass('box_clone', 'Drupal\box_clone\Form\BoxCloneForm');
  }
}

/**
 * Implements hook_entity_operation().
 */
function box_clone_entity_operation(EntityInterface $entity) {
  $operations = [];

  // Only add an operation for box entities.
  /** @var \Drupal\box\Entity\BoxInterface $entity */
  $entity_type_id = $entity->getEntityTypeId();
  if (!in_array($entity->getEntityTypeId(), ['box', 'biox'])) {
    return $operations;
  }

  if (!_box_clone_has_clone_permission($entity)) {
    return $operations;
  }

  $operations['duplicate'] = [
    'title' => t('Duplicate'),
    'weight' => '100',
    'url' => Url::fromRoute("box_clone.{$entity_type_id}.clone", [$entity_type_id => $entity->id()]),
  ];

  return $operations;
}

/**
 * Determine if the current user has permission to clone a specified box.
 *
 * @param \Drupal\box\Entity\BoxInterface $box
 *   The box to examine.
 *
 * @return bool
 *   TRUE or FALSE
 */
function _box_clone_has_clone_permission(BoxInterface $box) {
  $current_user = \Drupal::currentUser();
  $bundle = $box->bundle();
  $etid = $box->getEntityTypeId()();
  if ($current_user->hasPermission("clone {$bundle} {$etid}")) {
    if ($box->access('create')) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Get the label of a cloned box.
 *
 * @param \Drupal\box\Entity\Box $box
 *   The box entity.
 * @param string $langcode
 *   The language to use.
 *
 * @return \Drupal\Component\Render\FormattableMarkup
 *   The cloned box label.
 */
function box_clone_get_default_label(Box $box, $langcode = NULL): FormattableMarkup {
  $bc_config = \Drupal::config('box_clone.settings');
  $cloned_box_title = $bc_config->get('cloned_box_title');
  return new FormattableMarkup($cloned_box_title, ['@box_title' => $box->label()]);
}
