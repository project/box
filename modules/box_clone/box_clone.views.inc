<?php

/**
 * @file
 * Provide views data for box_clone.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function box_clone_views_data_alter(&$data) {
  $data['box']['clone_link'] = [
    'field' => [
      'title' => t('Add clone link'),
      'help' => t('Provide a clone link to the Box.'),
      'id' => 'box_clone_link',
    ],
  ];
  $data['biox']['clone_link'] = [
    'field' => [
      'title' => t('Add clone link'),
      'help' => t('Provide a clone link to the Biox.'),
      'id' => 'box_clone_link',
    ],
  ];
}
